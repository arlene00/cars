from django.urls import path
from . import views

urlpatterns = [
    path('index', views.index, name="car_list"),
    path('car/edit/<int:id>', views.edit),
    path('car/delete/<int:id>', views.delete, name="car_delete"),
    path('car/swap/', views.swap, name="car_swap"),
]
