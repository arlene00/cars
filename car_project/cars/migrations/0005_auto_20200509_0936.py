# Generated by Django 3.0.3 on 2020-05-09 01:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cars', '0004_auto_20200508_1652'),
    ]

    operations = [
        migrations.AlterField(
            model_name='car',
            name='sequence',
            field=models.FloatField(null=True, unique=True),
        ),
    ]
