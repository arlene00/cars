from django.shortcuts import render, redirect
from django.http import JsonResponse

from django.db.models import Max

from .models import Car
from .forms import CarForm


def index(request):
    form = CarForm()
    if request.method == 'POST':
        form = CarForm(request.POST)
        if form.is_valid():
            # get max value of sequence
            max_sequence = Car.objects.aggregate(Max('sequence'))
            car = form.save(commit=False)
            car.sequence = max_sequence['sequence__max'] + 1
            car.save()

    cars = Car.objects.all().order_by('sequence')
    return render(request, 'index.html', {'cars': cars, 'form': form})


def delete(request, id):
    car = Car.objects.get(id=id)
    car.delete()
    return redirect('/index')


def edit(request, id):
    pass


def swap(request):
    car_id = request.GET.get('car_id', None)
    front_car_position = request.GET.get('front_car_position', None)

    data = {
        'message': 'Invalid',
    }

    if car_id and front_car_position:
        # Get car to move
        car_move = Car.objects.get(pk=car_id)

        # Get car target destination
        try:
            car_destination = Car.objects.get(name=front_car_position)
        except Car.DoesNotExist:
            data['message'] = 'No such car name'
            return JsonResponse(data)

        # Store destination sequence
        new_seq = car_destination.sequence

        # Get adjacent car of target destination
        next_car = Car.objects.filter(sequence__gt=car_destination.sequence).order_by('sequence').first()

        # Update the sequence of the destination car
        car_destination.sequence = (car_destination.sequence + next_car.sequence) / 2
        car_destination.save()

        # Move the car into the new position
        car_move.sequence = new_seq
        car_move.save()

        data['message'] = 'Success'

    return JsonResponse(data)
