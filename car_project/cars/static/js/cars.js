$(function() {
    doSwap = function(carId) {
        var frontCarPosition = $('#swap_' + carId).val()
        $.ajax({
            headers: {
                'Content-Type':'application/json',
                'X-CSRFToken': getCookie('csrftoken')
            },
            url: '/car/swap/',
            type: 'GET',
            data: {
                'car_id': carId,
                'front_car_position': frontCarPosition
            },
            dataType: 'json',
            success: function (data) {
                if (data.message === 'Success') {
                    location.reload()
                } else {
                    alert(data.message)
                }
            }
        });
    }

    getCookie = function(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
});
