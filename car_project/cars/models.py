from django.db import models


class Car(models.Model):
    # Set name to true for reference to move car
    name = models.CharField(max_length=15, null=False, unique=True)
    color = models.CharField(max_length=15, null=False)
    sequence = models.FloatField(null=True, unique=True)

    def __str__(self):
        return self.name
